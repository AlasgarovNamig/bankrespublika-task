package com.br.task.service;


import com.br.task.service.dto.TaskRequestDto;
import com.br.task.service.dto.TaskResponseDto;
import com.br.task.service.dto.TaskStatusUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaskService {

        void saveTask(TaskRequestDto taskDto, String token);
        public List<TaskResponseDto> getStudentTask(String token);
        public List<TaskResponseDto> getAllTask();
        public void updateTaskStatus(Long id, TaskStatusUpdateDto taskStatusUpdateDto,String token);
}
