package com.br.task.security;


import com.br.task.config.CustomSpringSecurityUser;
import com.br.task.model.User;
import com.br.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.trace("Authenticating: {}", email);

        return userRepository.findByEmail(email)
                .map(this::createSpringSecurityUser)
                .orElseThrow(() ->
                        new UsernameNotFoundException(
                                String.format("User %s was not found in the database", email)));
    }
    private CustomSpringSecurityUser createSpringSecurityUser(User user) {

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getName()));
        return new CustomSpringSecurityUser(user.getEmail(),
                user.getPassword(),
                authorities

        );
    }

//    private void checkUserProfileStatus(User user) throws UserIsNotActiveException {
//        if (user.getStatus() != UserStatus.ACTIVE) {
//            throw new UserIsNotActiveException();
//        }
}
