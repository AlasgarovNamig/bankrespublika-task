package com.br.task.service;


import com.br.task.model.Enum.TaskStatus;
import com.br.task.model.Enum.UserStatus;
import com.br.task.model.Task;

import com.br.task.model.User;
import com.br.task.repository.TaskRepository;
import com.br.task.repository.UserRepository;
import com.br.task.security.JwtService;
import com.br.task.service.dto.TaskRequestDto;
import com.br.task.service.dto.TaskResponseDto;
import com.br.task.service.dto.TaskStatusUpdateDto;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final JwtService jwtService;


    @Override
    public void saveTask(TaskRequestDto taskDto, String token) {
        Task task = new Task();
//        Claims claims =jwtService.parseToken(token.split(" ")[1].trim());
//        User teacherUser =userRepository.findByEmail(claims.get("sub",String.class)).get();
        User teacherUser = returnUserObjectByToken(token);
        userRepository.findById(taskDto.getAssignedUserId()).ifPresent(student -> {
            if (student.getRaiting() > 0) {
                task.setAssignedUserId(student);
            } else {
                throw new RuntimeException();
            }
        });
        task.setCreatedUserId(teacherUser);
        task.setDescription(taskDto.getDescription());
        task.setDeadlineDate(stringDeadlineDateConvertToDate(taskDto.getDeadlineDate()));
        taskRepository.save(task);

    }


    @Override
    public List<TaskResponseDto> getStudentTask(String token) {
//        Claims claims =jwtService.parseToken(token.split(" ")[1].trim());
//        User studentUser= userRepository.findByEmail(claims.get("sub",String.class)).get();
        User studentUser = returnUserObjectByToken(token);
        List<Task> taskList = taskRepository.findAllByAssignedUserId(studentUser.getId());
//        List<TaskResponseDto> taskResponseDtos = new ArrayList<>();
//        for (Task task : taskList) {
//            TaskResponseDto taskResponseDto = new TaskResponseDto(task.getDescription(),
//                    task.getCreationDate(), task.getDeadlineDate(), task.getCreatedUserId().getId(),
//                    task.getAssignedUserId().getId(), task.getStatus());
//            taskResponseDtos.add(taskResponseDto);
//
//        }
        return taskListConvertTaskResponseDtoList(taskList);
    }

    @Override
    public List<TaskResponseDto> getAllTask() {
        List<Task> taskList = taskRepository.findAll();
//        List<TaskResponseDto> taskResponseDtos = new ArrayList<>();
//        for (Task task : taskList) {
//            TaskResponseDto taskResponseDto = new TaskResponseDto(task.getDescription(),
//                    task.getCreationDate(), task.getDeadlineDate(), task.getCreatedUserId().getId(),
//                    task.getAssignedUserId().getId(), task.getStatus());
//            taskResponseDtos.add(taskResponseDto);
//
//        }
        return taskListConvertTaskResponseDtoList(taskList);
    }

    @Override
    public void updateTaskStatus(Long id, TaskStatusUpdateDto taskStatusUpdateDto, String token) {
//        Claims claims =jwtService.parseToken(token.split(" ")[1].trim());
//        User studentUser= userRepository.findByEmail(claims.get("sub",String.class)).get();
        User studentUser = returnUserObjectByToken(token);
        taskRepository.findById(id).ifPresent(task -> {
                    if (task.getAssignedUserId().getId() == studentUser.getId()) {
                        task.setStatus(taskStatusUpdateDto.getStatus());
                        if (task.getDeadlineDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(LocalDate.now())) {
                            User user = userRepository.findById(task.getAssignedUserId().getId()).get();
                            if (user.getRaiting() < 1) {
                                user.setUserStatus(UserStatus.INACTIVE);
                            }
                            user.setRaiting(user.getRaiting() - 1);
                            userRepository.save(user);
                        }
                        taskRepository.save(task);
                    }

                }
        );
    }

    private List<TaskResponseDto> taskListConvertTaskResponseDtoList(List<Task> taskList){
        List<TaskResponseDto> taskResponseDtos = new ArrayList<>();
        for (Task task : taskList) {
            TaskResponseDto taskResponseDto = new TaskResponseDto(task.getDescription(),
                    task.getCreationDate(), task.getDeadlineDate(), task.getCreatedUserId().getId(),
                    task.getAssignedUserId().getId(), task.getStatus());
            taskResponseDtos.add(taskResponseDto);

        }
        return taskResponseDtos;
    }
    private User returnUserObjectByToken(String token) {
        Claims claims = jwtService.parseToken(token.split(" ")[1].trim());
        return userRepository.findByEmail(claims.get("sub", String.class)).get();
    }

    private Date stringDeadlineDateConvertToDate(String deadlineDate) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(deadlineDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


}
