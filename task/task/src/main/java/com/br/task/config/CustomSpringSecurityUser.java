package com.br.task.config;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.List;

public class CustomSpringSecurityUser extends  User{
    private static final long serialVersionUID = 3522416053866116034L;

    @Getter
    @Setter
    private Long  id;


    public CustomSpringSecurityUser(String email, String password, List<SimpleGrantedAuthority> authorities) {
        super(email, password, authorities);
    }
}
