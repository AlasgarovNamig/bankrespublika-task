package com.br.task.service.dto;


import com.br.task.config.validation.ValidPassword;
import com.br.task.model.Enum.UserStatus;
import com.br.task.model.Role;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "password")
public class SignUpDto {
    @NotBlank
    private String name;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    @ValidPassword
    private String password;

    @NotBlank
    private String role;
}
