package com.br.task.model;

import com.br.task.model.Enum.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = Task.TABLE_NAME)
public class Task {
    public static final String TABLE_NAME = "mission";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(updatable = false,name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "deadline_date")
    private Date deadlineDate;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "created_user_id", referencedColumnName = "id")
    private User createdUserId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "assigned_user_id", referencedColumnName = "id")
    private User assignedUserId;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @PrePersist
    public void prePersist() {
        creationDate = LocalDateTime.now();
        status = TaskStatus.PENDING;

    }

}
