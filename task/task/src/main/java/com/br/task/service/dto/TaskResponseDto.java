package com.br.task.service.dto;

//import com.br.task.model.MissionStatus;

import com.br.task.model.Enum.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
public class TaskResponseDto {
    private String description;
    private LocalDateTime creationDate;
    private Date deadline_date;
    private Long created_user_id;
    private Long assigned_user_id;
    private TaskStatus status;
}
