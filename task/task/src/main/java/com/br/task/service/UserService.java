package com.br.task.service;


import com.br.task.service.dto.SignUpDto;

public interface UserService {
    void signUp(SignUpDto signUpDto);
}
