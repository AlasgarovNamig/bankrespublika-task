package com.br.task.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TaskRequestDto {

    private String description;
    private String deadlineDate;
    private Long assignedUserId;
}
