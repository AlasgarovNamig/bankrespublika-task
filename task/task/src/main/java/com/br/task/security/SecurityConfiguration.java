package com.br.task.security;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class})
//LoggingTcpConfiguration.class
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/auth/sign-in")
                .permitAll()
                .and()
                .authorizeRequests().antMatchers(HttpMethod.POST, "/task/create").hasAuthority("TEACHER")
                .and()
                .authorizeRequests().antMatchers("/auth/sign-up").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/task/student/**").hasAuthority("STUDENT")
                .and()
                .authorizeRequests().antMatchers(HttpMethod.POST, "/task/all").hasAuthority("TEACHER");

        super.configure(http);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
