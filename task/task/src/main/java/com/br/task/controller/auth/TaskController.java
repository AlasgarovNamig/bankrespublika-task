package com.br.task.controller.auth;


import com.br.task.service.TaskServiceImpl;
import com.br.task.service.dto.TaskRequestDto;
import com.br.task.service.dto.TaskResponseDto;
import com.br.task.service.dto.TaskStatusUpdateDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskServiceImpl taskService;



    @PostMapping("/create")
    public ResponseEntity<Void> createTask(@RequestBody @Valid TaskRequestDto taskDto )  {
        String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest().getHeader("Authorization");
       taskService.saveTask(taskDto,token);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/students")
    public List<TaskResponseDto> getSpecialStudentTask(){
        String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest().getHeader("Authorization");
        return taskService.getStudentTask(token);
    }
    @GetMapping("/all")
    public List<TaskResponseDto> getAllTask(){
        return taskService.getAllTask();
    }

    @PutMapping("student/{id}")
    public void updateStatusTask(@PathVariable Long id, @RequestBody TaskStatusUpdateDto taskStatusUpdateDto) {
        String token= ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest().getHeader("Authorization");
        taskService.updateTaskStatus(id,taskStatusUpdateDto,token);
    }


}
