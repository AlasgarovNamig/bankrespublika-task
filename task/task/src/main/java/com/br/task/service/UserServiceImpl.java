package com.br.task.service;


import com.br.task.model.Role;
import com.br.task.model.User;
import com.br.task.repository.RoleRepository;
import com.br.task.repository.UserRepository;
import com.br.task.service.dto.SignUpDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public void signUp(SignUpDto signUpDto) {
        userRepository.findByEmail(signUpDto.getEmail())
                .ifPresent(user -> {
//                    throw new EmailAlreadyUsedException(signUpDto.getEmail());
                });
        User user = createUserEntityObject(signUpDto);
        userRepository.save(user);
    }


    private User createUserEntityObject(SignUpDto signUpDto) {
        User user = new User();
        user.setName(signUpDto.getName());
        user.setEmail(signUpDto.getEmail());
        roleRepository.findById(signUpDto.getRole()).ifPresent(role -> user.setRole(role));
        user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
        return user;
    }
}
