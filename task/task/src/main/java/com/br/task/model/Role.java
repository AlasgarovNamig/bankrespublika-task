package com.br.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = Role.TABLE_NAME )

public class Role {
    public static final String TABLE_NAME = "roles";
    @Id
    private  String name;

    @Override
    public String toString() {
        return  name;
    }
}
