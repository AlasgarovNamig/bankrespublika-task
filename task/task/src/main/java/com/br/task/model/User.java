package com.br.task.model;

import com.br.task.model.Enum.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = User.TABLE_NAME)
public class User {
    public static final String TABLE_NAME = "users";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "fathername")
    private String fatherName;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "role", referencedColumnName = "name")
    private Role role;

    @Min(0)
    @Max(10)
    @Column(name = "raiting")
    private int raiting;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private UserStatus userStatus;




}
