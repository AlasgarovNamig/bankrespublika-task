# Task Information

1)create a "users" table and "roles" tables and make relation between tables. A user must have a single role;

2)create 3 predefined roles (admin,student, teacher)

3)write a rest api that takes username and password as arguments and returns jwt token (in jwt token payload add username and role);

4)create api named "/add/user" to add a new user with proper validation, only a user with admin role can access to this service.

5)create a few users with role of teacher using api of "/add/user" defined in (4)
5)create a few users with role of student using api of "/add/user" defined in (4) 

6)create "tasks" table 

7)create api for the users with teacher role to add tasks to the users with student roles; 

8)create api for a user with student role to see tasks assigned to him 

9)create api for a user with student role to change status of task to "success" after finishing if deadline is passed decrease rank 1,
 starting rank for any user with student role is 10 and if a user with student role gets rank of 0 
he can not be assigned any tasks: 

10)create a api for a user teacher role to retrieve tasks with statuses 

11)Create custom exceptions and handle these. 

12)Log request and response to file. 